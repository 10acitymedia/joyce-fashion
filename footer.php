<!--// The start of the footer -->

	<div class="container">
		<div class="row" id="row-footer">
			<div class="col-12 col-sm-12 col-md-12 col-lg-12" id="footer-p">
				<p><i>© Joyce Gereige | Designed by <a href="http://phaseshiftmedia.com" target="_blank">PhaseShiftMedia</a></i></p>
			</div>
		</div>
	</div>


<!--// End of footer -->