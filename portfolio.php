<!DOCTYPE html>
<html>
<head>
	<title>Portfolio</title>
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" type="text/css" href="style/css/celebrity.css">
    <link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="style/css/responsive.css">
    <link rel="stylesheet" type="text/css" href="style/css/social-box.css">
    <link rel="stylesheet" type="text/css" href="style/css/nav.css">
    <link rel="stylesheet" type="text/css" href="style/css/footer.css">
    <link href="https://fonts.googleapis.com/css?family=Didact+Gothic" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Playfair+Display" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Taviraj" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
</head>
<body>

    <?php require_once 'social-icon.php'; ?>
	<?php require_once "nav.php"; ?>


        <div class="container">
            <div class="row">
                <div id="category-name" class="col-3">
                    <h3 class="text-left"><i>Portfolio</i></h3>
                    <div id="line1"></div>
                </div>
            </div>
        </div>
    <div class="container-fluid" id="main-container">
        <div class="row">
            <div class="col-12">
                <div class="container-fluid">
                    <div class="row row_without_padding">
                        <?php 

                            $conn = mysqli_connect("s201.loopia.se", "taskovic@t34555", "opremdobro", "taskovic_com");
                            //$conn = mysqli_connect("localhost","root","","joyce");
                            $sql = "SELECT * FROM post ORDER BY id DESC";
                            $query = mysqli_query($conn, $sql);

                            while($row = mysqli_fetch_assoc($query)){
                        ?>
                            <div id="holder" class="col-xs-5ths col-sm-5ths col-md-5ths col-lg-5ths">
                            <img class="first-fade img-fluid" src="<?php echo $row['img'];?>"/>
                            <a style="text-decoration: none; color: #000;" href="show-page.php?post=<?php echo $row['id']; ?>"><div class="cover"><h1 class="align-middle"><?php echo $row['title'];?></h1></div></a>
                            </div>
                        <?php } ?>
                </div>
            </div>
        </div>
    </div>
</div>


    <?php require_once "footer.php"; ?>

    
    <script src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.2.1.min.js"></script>
    <script type="text/javascript" src="style/js/showImages.js"></script>
    <script type="text/javascript" src="bootstrap/js/bootstrap.min.js"></script>
</body>
</html>