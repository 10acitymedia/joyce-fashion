<!--// Social media icon above nav bar -->

<div class="container" id="soc-container">
	<div class="row">
		<div class="col-12" id="soc-box">
			<a href="#" target="_blank"><i class="fa fa-envelope-o" aria-hidden="true"></i></a>
			<a href="https://www.linkedin.com/in/joyce-gereige-68149597/" target="_blank"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
			<a href="https://www.instagram.com/joycegereige/" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i></a>
			<a href="https://www.facebook.com/joycegereige/?ref=bookmarks" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a>
		</div>
	</div>
</div>

<!--// End social media box -->