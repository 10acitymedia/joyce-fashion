<!-- nav -->

<div class="container-fluid" id="full-width-nav">
	<div class="row">
		<div class="container" id="con">
			<nav class="navbar navbar-expand-lg navbar-light" id="conn">
		  		
				<!--// Logo bar -->
				<div>
		  			<a class="navbar-brand" href="home" id="logo">JOYCE GEREIGE</a>
		  			<span id="fashion-syilist">Fashion Stylist</span>
		  		</div>
	  		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
	    		<span class="navbar-toggler-icon"></span>
	  		</button>

		  		<!--// End logo bar -->



		  		<!--// The part where the menu is -->

			  	<div class="collapse navbar-collapse" id="navbarNavDropdown">
			    <ul class="navbar-nav ml-auto">
			      <li class="nav-item">
			        <a class="nav-link text-center" href="about">About</a>
			      </li>
				  <li class="nav-item">
			      	<a class="nav-link text-center" href="portfolio">Portfolio</a>
			      </li>
				  <li class="nav-item">
			      	<a class="nav-link text-center" href="music-videos">Videos</a>
			      </li>
			      <li class="nav-item">
			      	<a class="nav-link text-center" href="press">Press</a>
			      </li>
			      <li class="nav-item">
			        <a class="nav-link text-center" href="contact">Contact</a>
			      </li>
			    </ul>
			  </div>

				  <!--// End of menu -->
			</nav>
		</div>
	</div>
</div>