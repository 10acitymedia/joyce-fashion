<?php
session_start();

if(isset($_SESSION['admin'])){

}else{
	header("Location: admin.php");
}


?>

<!DOCTYPE html>
<html>
<head>
	<title>Dashboard Joyce</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="style/css/dashboard.css">
	<link href="https://fonts.googleapis.com/css?family=Pompiere" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Didact+Gothic" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<script src="bootstrap/js/bootstrap.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.13.0/popper.min.js"></script>
	<link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
	<link href="style/css/fileinput.css" media="all" rel="stylesheet" type="text/css" />
    <script src="style/js/fileinput.min.js" type="text/javascript"></script>
</head>
<body>

	<div class="container-fluid" id="container-form">
		<div class="row">
			<div class="col-12 col-sm-12 col-md-12 col-lg-12">
				<div class="row" id="ukupno">
					<div class="col-12">
						<h1 class="text-center">Joyce Dashboard</h1><a class="pull-right" href="logout.php">Log out</a>
						<hr>
					</div>
				</div>
				<div class="row" id="form">
					<div class="col-12 col-sm-12 col-md-12 col-lg-12">
						<div class="row">
							<div class="col-12 col-sm-10 offset-sm-1 col-md-4 offset-md-2 col-lg-4 offset-lg-2">
								<h2 class="text-center" id="post">Add Post</h2><br>
								<form action="debug.php" method="POST" enctype="multipart/form-data">
									<div class="form-group">
										<label for="exampleInputEmail1">Title *</label>
		    							<input type="text" name="title" class="form-control" id="title" aria-describedby="emailHelp" placeholder="Enter the post title..." required=""><br>
		    							<label for="exampleInputEmail1">Description *</label>
		    							<textarea rows="4" name="description" class="form-control" id="description" aria-describedby="emailHelp" placeholder="Describe this post..." required=""></textarea><br>
		    							<label for="exampleInputEmail1">Front image *</label>
		    							<input type="file" name="frontimage" class="form-control" id="frongImage" aria-describedby="emailHelp"><br>
		    							<label for="exampleInputEmail1">Images in the post *</label>
		    							<center>
											<div class="form-group">
												<input type="file" id="file-1" class="file" multiple name="image[]">
											</div>
										</center>
										<script type="text/javascript">
											$('#file-1').fileinput({
												uploadUrl:'#',
												allowedFileExtension:['jpg','png','gif'],
												overwriteInitial:false,
												maxFileSize:1000,
												maxFileNum:5,
											});
										document.getElementsByClassName('file-drop-zone-title')[0].innerHTML = "Browse for images...";
										</script><br>
		    							<input type="button" value="Add Post" class="btn btn-lg btn-primary" data-toggle="modal" data-target="#exampleModal3">
		    							<div class="modal" id="exampleModal3" tabindex="-1" role="dialog">
										  <div class="modal-dialog" role="document">
										    <div class="modal-content">
										      <div class="modal-header">
										        <h5 class="modal-title">Add Post</h5>
										        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
										          <span aria-hidden="true">&times;</span>
										        </button>
										      </div>
										      <div class="modal-body">
										        <p id="changeText3">Click the add button to confirm this action. If you wish to cancel it, click close.</p>
										      </div>
										      <div class="modal-footer">
										        <button type="submit" name="submit" class="btn btn-primary">Add</button>
										        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
										      </div>
										    </div>
										  </div>
										</div>
									</div>
								</form>
							</div>

									<?php
										$conn = mysqli_connect("s201.loopia.se", "taskovic@t34555", "opremdobro", "taskovic_com");
						  				//$conn = mysqli_connect("localhost","root","","joyce");
										$messagez = "";

										if(isset($_POST["message"])){
											$messagez = $_POST["message"];
											$sql = "SELECT email FROM mail";
											$query = mysqli_query($conn,$sql);

											while($row = mysqli_fetch_assoc($query)){ 	
											$to      =  $row["email"];
											$subject = 'Joyce-Newsletter update!';
											$message =  $messagez;
											$headers = 'From: joyce@gereige.com' . "\r\n";
											mail($to, $subject, $message, $headers);
										}
									}
									?>

									<?php 
										  	$conn = mysqli_connect("s201.loopia.se", "taskovic@t34555", "opremdobro", "taskovic_com");
						  					//$conn = mysqli_connect("localhost","root","","joyce");

										if(isset($_POST['trends'])){
										$trend = mysqli_real_escape_string($conn, $_POST['trends']);
										$sql1 = "UPDATE trend SET trends='$trend' WHERE id=1";
										$query1 = mysqli_query($conn,$sql1);
											}

										$sql = "SELECT trends FROM trend WHERE id=1";

										$query = mysqli_query($conn,$sql);

										while($row = mysqli_fetch_assoc($query)){ 		

									?>
								
							<div class="col-12 col-sm-10 offset-sm-1 col-md-4 offset-md-0 col-lg-4 offset-lg-0">
								<h2 class="text-center" id="trend">Update Trend</h2><br>
								<form action="" method="POST">
									<div class="form-group">
		    							<label for="exampleInputEmail1">What's trending now?</label>
		    							<textarea rows="4" name="trends" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter text..."><?php echo $row["trends"]?></textarea><br>
		    							<button type="button" onclick="myFunction()" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
											  Update
											</button>
										<div class="modal" id="exampleModal" tabindex="-1" role="dialog">
										  <div class="modal-dialog" role="document">
										    <div class="modal-content">
										      <div class="modal-header">
										        <h5 class="modal-title">Are you sure you want to make these changes?</h5>
										        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
										          <span aria-hidden="true">&times;</span>
										        </button>
										      </div>
										      <div class="modal-body">
										        <p id="changeText"></p>
										      </div>
										      <div class="modal-footer">
										        <button type="submit" name="submit" class="btn btn-primary">Save changes</button>
										        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
										      </div>
										    </div>
										  </div>
										</div>
									</div>
								</form>
								<h2 class="text-center" id="emailz">Send a newsletter</h2><br>
								<form action="" method="POST">
									<div class="form-group">
		    							<label for="exampleInputEmail1">What's on your mind?</label>
		    							<textarea rows="4" name="message" class="form-control" id="exampleInputEmail2" aria-describedby="emailHelp" placeholder="Say hello to your subscribers..." required=""></textarea><br>
		    							<button type="button" onclick="myFunction2()" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal2">
											  Send Email
											</button>
										<div class="modal" id="exampleModal2" tabindex="-1" role="dialog">
										  <div class="modal-dialog" role="document">
										    <div class="modal-content">
										      <div class="modal-header">
										        <h5 class="modal-title">Are you sure you want to send this Email?</h5>
										        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
										          <span aria-hidden="true">&times;</span>
										        </button>
										      </div>
										      <div class="modal-body">
										        <p id="changeText2"></p>
										      </div>
										      <div class="modal-footer">
										        <button type="submit" name="submit" class="btn btn-primary">Send</button>
										        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
										      </div>
										    </div>
										  </div>
										</div>
									</div>
								</form>
								</div>
								<div class="col-12 col-sm-10 offset-sm-1 col-md-4 offset-md-2 col-lg-4 offset-lg-2">
									<h2 class="text-center" id="post">Add Press Post</h2><br>
									<form action="debug2.php" method="POST" enctype="multipart/form-data">
										<div class="form-group">
											<label for="exampleInputEmail1">Title *</label>
			    							<input type="text" name="title2" class="form-control" id="title2" aria-describedby="emailHelp" placeholder="Enter the post title..." required=""><br>
			    							<label for="exampleInputEmail1">Description *</label>
			    							<textarea rows="4" name="description2" class="form-control" id="description2" aria-describedby="emailHelp" placeholder="Describe this post..." required=""></textarea><br>
			    							<label for="exampleInputEmail1">Front image *</label>
			    							<input type="file" name="frontimage2" class="form-control" id="frontImage2" aria-describedby="emailHelp"><br>
			    							<label for="exampleInputEmail1">Images in the post *</label>
			    							<center>
												<div class="form-group">
													<input type="file" id="file-2" class="file" multiple name="pimage[]">
												</div>
											</center>
											<script type="text/javascript">
												$('#file-2').fileinput({
													uploadUrl:'#',
													allowedFileExtension:['jpg','png','gif'],
													overwriteInitial:false,
													maxFileSize:1000,
													maxFileNum:5,
												});
											document.getElementsByClassName('file-drop-zone-title')[0].innerHTML = "Browse for images...";
											</script><br>
			    							<input type="button" value="Add Post" class="btn btn-lg btn-primary" data-toggle="modal" data-target="#exampleModal4">
			    							<div class="modal" id="exampleModal4" tabindex="-1" role="dialog">
											  <div class="modal-dialog" role="document">
											    <div class="modal-content">
											      <div class="modal-header">
											        <h5 class="modal-title">Add Post</h5>
											        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
											          <span aria-hidden="true">&times;</span>
											        </button>
											      </div>
											      <div class="modal-body">
											        <p id="changeText3">Click the add button to confirm this action. If you wish to cancel it, click close.</p>
											      </div>
											      <div class="modal-footer">
											        <button type="submit" name="submit" class="btn btn-primary">Add</button>
											        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
											      </div>
											    </div>
											  </div>
											</div>
										</div>
									</form>
								</div>
							<?php } ?>

							<div class="col-12 col-sm-10 offset-sm-1 col-md-4 offset-md-0 col-lg-4 offset-lg-0">
								<h1 class="text-center" style="margin-bottom: 20px;">Add Video</h1>
								<form action="video.php" method="POST">
									<label for="video">Video link</label>
									<input type="text" name="video" id="video" class="form-control">
									<input type="button" style="margin-top: 15px;" value="Add video" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal10">

									<div class="modal" id="exampleModal10" tabindex="-1" role="dialog">
									  <div class="modal-dialog" role="document">
									    <div class="modal-content">
									      <div class="modal-header">
									        <h5 class="modal-title">Add Post</h5>
									        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
									          <span aria-hidden="true">&times;</span>
									        </button>
									      </div>
									      <div class="modal-body">
									        <p id="changeText3">Click the add button to confirm this action. If you wish to cancel it, click close.</p>
									      </div>
									      <div class="modal-footer">
									        <button type="submit" name="submit" class="btn btn-primary">Add</button>
									        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
									      </div>
									    </div>
									  </div>
									</div>
								</form>
							</div>
							</div>
				<script type="text/javascript">
					function myFunction(){
				var x = document.getElementById('exampleInputEmail1').value;
				document.getElementById('changeText').innerHTML = x;
				}
					function myFunction2(){
				var x = document.getElementById('exampleInputEmail2').value;
				document.getElementById('changeText2').innerHTML = x;
				}
				</script>

				<div class="row">
					<div class="col-12 col-sm-12 col-md-6 offset-md-3 col-lg-5 offset-lg-3">
						<h2 class="text-center" id="delete">Delete Posts</h2><br>
						<table class="table">
						  	<thead>
						    	<tr>
						      		<th scope="col">Description</th>
						      		<th scope="col">Delete</th>
						    	</tr>
						  	</thead>
						  	<tbody>
						  		 <?php 

                            $conn = mysqli_connect("s201.loopia.se", "taskovic@t34555", "opremdobro", "taskovic_com");
						  	//$conn = mysqli_connect("localhost","root","","joyce");
                            $sql = "SELECT * FROM post";
                            $query = mysqli_query($conn,$sql);

                            while($row = mysqli_fetch_assoc($query)){

                             ?>
						    	<tr>
						    		<form action="edit-post.php" method="post">
						    			<input type="hidden" name="id" value="<?=$row['id']?>">
						      			<td><textarea cols="70" rows="4" name="description"><?=$row['description']?></textarea></td>
						      			<td><input class="btn btn-primary" type="submit" value="EDIT"></td>
						      		</form>

						      		<td><a class="btn btn-danger" href="delete.php?id=<?php echo $row['id']; ?>">DELETE</a></td>
						    	</tr>
						    	<?php } ?>
						    </tbody>
						</table>
					</div>
				</div>
 
 
				<div class="row" style="margin-top: 60px;">
					<div class="col-12 col-sm-12 col-md-6 offset-md-3 col-lg-5 offset-lg-3">
						<h2 class="text-center" id="delete">Delete Press Posts</h2><br>
						<table class="table">
						  	<thead>
						    	<tr>
						      		<th scope="col">Description</th>
						      		<th scope="col">Delete</th>
						    	</tr>
						  	</thead>
						  	<tbody>
						  		 <?php 

                            $conn = mysqli_connect("s201.loopia.se", "taskovic@t34555", "opremdobro", "taskovic_com");
						  	//$conn = mysqli_connect("localhost","root","","joyce");
                            $sql = "SELECT * FROM press";
                            $query = mysqli_query($conn,$sql);

                            while($row = mysqli_fetch_assoc($query)){

                             ?>
						    	<tr>
						      		<form action="edit-press.php" method="post">
						    			<input type="hidden" name="id" value="<?=$row['id']?>">
						      			<td><textarea cols="70" rows="4" name="description"><?=$row['description']?></textarea></td>
						      			<td><input class="btn btn-primary" type="submit" value="EDIT"></td>
						      		</form>

						      		<td><a class="btn btn-danger" href="delete2.php?id=<?php echo $row['id']; ?>">DELETE</a></td>
						    	</tr>
						    	<?php } ?>
						    </tbody>
						</table>
					</div>
				</div>


				<div class="row" style="margin-top: 60px;">
					<div class="col-12 col-sm-12 col-md-6 offset-md-3 col-lg-5 offset-lg-3">
						<h2 class="text-center" id="delete">Delete Video Posts</h2><br>
						<table class="table">
						  	<thead>
						    	<tr>
						      		<th scope="col">Id</th>
						      		<th scope="col">Delete</th>
						    	</tr>
						  	</thead>
						  	<tbody>
						  		 <?php 

                            $conn = mysqli_connect("s201.loopia.se", "taskovic@t34555", "opremdobro", "taskovic_com");
						  	//$conn = mysqli_connect("localhost","root","","joyce");
                            $sql = "SELECT * FROM video";
                            $query = mysqli_query($conn,$sql);

                            while($row = mysqli_fetch_assoc($query)){

                             ?>
						    	<tr>
						      		<td><?php echo $row['id']?></td>
						      			<td><a class="btn btn-danger" href="deleteVideo.php?id=<?php echo $row['id']; ?>">DELETE</a></td>
						    	</tr>
						    	<?php } ?>
						    </tbody>
						</table>
					</div>
				</div>


				<div class="row" id="subs">
					<div class="col-12 col-sm-12 col-md-6 offset-md-3 col-lg-6 offset-lg-3">
						<h2 class="text-center">Subscribers</h2>
						<table class="table" id="sub-table">
						  	<thead>
						    	<tr>
						      		<th scope="col">Id</th>
						      		<th scope="col">Email</th>
						      		<th scope="col">Delete</th>
						    	</tr>
						  	</thead>
						  	<tbody>
						  			
						  			<?php

						  			$conn = mysqli_connect("s201.loopia.se", "taskovic@t34555", "opremdobro", "taskovic_com");
								  	//$conn = mysqli_connect("localhost","root","","joyce");
		                            $sql = "SELECT * FROM mail ORDER BY id DESC";
		                            $query = mysqli_query($conn,$sql);

		                            while($row = mysqli_fetch_assoc($query)){ ?>

		                            <tr>
			                            <td><?=$row['id']?></td>
							  			<td><?=$row['email']?></td>
							  			<td><a class="btn btn-danger" href="deleteSub.php?id=<?=$row['id']?>">Delete Subscriber</a></td>
						  			</tr>

						  			<?php } ?>
				
						  	</tbody>
						</table>
					</div>	
				</div>
			</div>
		</div>
	</div>
</body>
</html>