<!DOCTYPE html>
<html>
<head>
	<title>Joyce Gereige</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="style/css/home.css">
	<link rel="stylesheet" type="text/css" href="style/css/nav-two.css">
	<link rel="stylesheet" type="text/css" href="style/css/footer.css">
	<link href="https://fonts.googleapis.com/css?family=Pompiere" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Didact+Gothic" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Playfair+Display" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Oranienbaum" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Taviraj" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<script src="bootstrap/js/bootstrap.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.13.0/popper.min.js"></script>
</head>
<body>

	<?php require_once 'nav-two.php'; ?>

	<!--// Start image slider -->

	<div class="container-fluid" id="slider">
		<div class="row">
			<div class="col-12 col-sm-12 col-md-12 col-lg-12" id="sub-container-fluid">
				<div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
				  <div class="carousel-inner">

				    <div class="carousel-item active" id="image-one">
				      <img id="one" class="d-block w-100 image" src="image/fashion-one.jpg" alt="First slide">
				    </div>
				    <div class="carousel-item" id="image-two">
				      <img id="two" class="d-block w-100 image" src="image/fashion-two.jpg" alt="Second slide">
				    </div>
				    <div class="carousel-item" id="image-213">
				      <img id="three" class="d-block w-100 image" src="image/fashion-three.jpg" alt="Third slide">
				    </div>
				    <div class="carousel-item" id="image-four">
				      <img id="four" class="d-block w-100 image" src="image/fashion-four.jpg" alt="Four slide">
				    </div>
				
				  </div>
				  <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
				    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
				    <span class="sr-only">Previous</span>
				  </a>
				  <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
				    <span class="carousel-control-next-icon" aria-hidden="true"></span>
				    <span class="sr-only">Next</span>
				  </a>
				</div>
			</div>
		</div>
	</div>


	<!--// End image slider -->


	<!--// Container with link image -->


	<div class="container" id="image-t">
		<div class="row">
			<div class="col-12 col-sm-12 parent">
				<img class="img-responsive" src="image/edit3.jpg">
				<a href="portfolio" class="hover">
					Portfolio
				</a>
			</div>
			<div class="col-12 col-sm-12 parent">
				<img class="img-responsive" src="image/edit4.jpg">
				<a href="music-videos" class="hover">
					Videos
				</a>
			</div>
			<div class="col-12 col-sm-12 parent">
				<img class="img-responsive" src="image/edit5.jpg">
				<a href="press" class="hover">
					Press
				</a>
			</div>
		</div>
	</div>


	<!--// End link image -->

	<!--// Container for feed, post, subscribe -->

	<script type="text/javascript">
	</script>

	<div class="container">
		<div class="row" id="post-row">
			<div class="col-12 col-sm-12 col-md-4 col-lg-4 text-left" id="subscribe">
				<form action="" method="POST">
					<div class="form-group">
						<input type="email" name="emailq" aria-describedby="emailHelp" id="email" placeholder="Enter email" required="">
					</div>
					<button type="button" class="btn btn-default" data-toggle="modal" data-target="#exampleModal5">SUBSCRIBE</button>
						<div class="modal" id="exampleModal5" tabindex="-1" role="dialog">
							  <div class="modal-dialog" role="document">
							    <div class="modal-content">
							      <div class="modal-header">
							        <h5 class="modal-title">Yay!</h5>
							          <span aria-hidden="true">&times;</span>
							        </button>
							      </div>
							      <div class="modal-body">
							        <p>You have successfully subscribed to the newsletter! </p>
							      </div>
							      <div class="modal-footer">
							        <button type="submit" name="submit" id="okButt" class="btn btn-primary" data-dismiss="modal">Ok</button>
							      </div>
							    </div>
							  </div>
							</div>				
					</form>
					<?php 
						$conn = mysqli_connect("s201.loopia.se", "taskovic@t34555", "opremdobro", "taskovic_com");
						$email = "";
						if(isset($_POST["emailq"]) && !empty($_POST["emailq"])){
						$email = $_POST["emailq"];
						$sql1 = "SELECT email FROM mail WHERE email = '$email'";
						$query1 = mysqli_query($conn,$sql1);
						$row_cnt = mysqli_num_rows($query1);
						if($row_cnt==0){
						$sql = "INSERT INTO mail (email) value ('$email')";
						$query = mysqli_query($conn,$sql);
							}
						}
				?>
				<div class="text-left" id="soc-icon">
					<a href="https://www.facebook.com/joycegereige/?ref=bookmarks" style="margin-left: 0px;" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a>
					<a href="https://www.instagram.com/joycegereige/" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i></a>
					<a href="https://www.linkedin.com/in/joyce-gereige-68149597/" target="_blank"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
					<a href="contact"><i class="fa fa-envelope-o" aria-hidden="true"></i></a>
				</div>

				<div class="container text-center" id="soc-container">
					<div class="row">
						<div class="col-12" id="soc-box">
							<a href="https://www.facebook.com/joycegereige/?ref=bookmarks" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a>
							<a href="https://www.instagram.com/joycegereige/" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i></a>
							<a href="https://www.linkedin.com/in/joyce-gereige-68149597/" target="_blank"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
							<a href="contact"><i class="fa fa-envelope-o" aria-hidden="true"></i></a>
						</div>
					</div>
				</div>
			</div>
			
			<div class="col-12 col-sm-12 col-md-4 offset-md-4 col-lg-4 offset-lg-4" id="now-tre">
				<h3 class="text-center" id="trend">Now Trending</h3>
				<div id="trend-line"></div>

				<?php 
					$conn = mysqli_connect("s201.loopia.se", "taskovic@t34555", "opremdobro", "taskovic_com");
					$sql = "SELECT trends FROM trend WHERE id=1";
					$query = mysqli_query($conn,$sql);

					while($row = mysqli_fetch_assoc($query)){ 		?>
			
				<p class="text-center" id="now-p"><?php echo $row["trends"]?></p>
			
			<?php } ?>
			
			</div>
		</div>
	</div>

	<div class="container-fluid" id="insta">
		<div class="row">
			<div class="col-12" id="insta-col">
				<!-- LightWidget WIDGET --><script src="//lightwidget.com/widgets/lightwidget.js"></script><iframe id="ajfrejm" src="//lightwidget.com/widgets/5651faf8a2015bf08bd85ef7f77bfb66.html" scrolling="no" allowtransparency="true" class="lightwidget-widget" style="width: 100%; border: 0; overflow: hidden;"></iframe>

				<script>

					$(window).on('load resize', function () {

    					var screenWidth = $(window).width();

						console.log(screenWidth);

						if(screenWidth <= 922){
							document.getElementById("ajfrejm").src = "//lightwidget.com/widgets/f7c36f2215d95080a2a3f5f4f906e0db.html";

						}

						if(screenWidth > 922){

							document.getElementById("ajfrejm").src = "//lightwidget.com/widgets/5651faf8a2015bf08bd85ef7f77bfb66.html";

						}
					});
				</script>

			</div>
		</div>
	</div>


	<!--// end post container -->

	
	<?php require_once 'footer.php'; ?>

	



	<!--<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/js/bootstrap.min.js" integrity="sha384-a5N7Y/aK3qNeh15eJKGWxsqtnX/wWdSZSKp+81YjTmS15nvnvxKHuzaWwXHDli+4" crossorigin="anonymous"></script>-->

</body>
</html>