<!DOCTYPE html>
<html>
<head>
	<title>Music Video</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="style/css/music-videos.css">
	<link rel="stylesheet" type="text/css" href="style/css/responsive.css">
	<link rel="stylesheet" type="text/css" href="style/css/social-box.css">
	<link rel="stylesheet" type="text/css" href="style/css/nav.css">
	<link rel="stylesheet" type="text/css" href="style/css/footer.css">
	<link href="https://fonts.googleapis.com/css?family=Pompiere" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Didact+Gothic" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Taviraj" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Playfair+Display" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<script src="bootstrap/js/bootstrap.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.13.0/popper.min.js"></script>
	<link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
</head>
<body>

	<?php require_once 'social-icon.php'; ?>
	<?php require_once 'nav.php'; ?>

	<div class="container">
        <div class="row">
            <div id="category-name" class="col-3">
                <h3 class="text-left"><i>Videos</i></h3>
                <div id="line"></div>
            </div>
        </div>
    </div>

	<div class="container-fluid">
		<div class="row" id="video-padd">
			
			<?php

			$conn = mysqli_connect("s201.loopia.se", "taskovic@t34555", "opremdobro", "taskovic_com");
			//$conn = mysqli_connect("localhost","root","","joyce");
			
			$sql = "SELECT * FROM video";
			$query = mysqli_query($conn, $sql);

			while($row = mysqli_fetch_assoc($query)){ ?>
				<div class="col-12 col-sm-12 offset-sm-0 col-md-6 col-lg-6 embed-responsive embed-responsive-16by9 video">
					<iframe class="embed-responsive-item classWithPad" src="<?=$row['video_link']?>" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
				</div>

			<?php }	?>

		</div>
	</div>


	<?php require_once 'footer.php'; ?>
	<!--
	<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/js/bootstrap.min.js" integrity="sha384-a5N7Y/aK3qNeh15eJKGWxsqtnX/wWdSZSKp+81YjTmS15nvnvxKHuzaWwXHDli+4" crossorigin="anonymous"></script>-->
</body>
</html>