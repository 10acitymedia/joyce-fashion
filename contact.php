<!DOCTYPE html>
<html>
<head>
	<title>Contact</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="style/css/contact.css">
	<link rel="stylesheet" type="text/css" href="style/css/social-box.css">
	<link rel="stylesheet" type="text/css" href="style/css/nav.css">
	<link rel="stylesheet" type="text/css" href="style/css/footer.css">
	<link href="https://fonts.googleapis.com/css?family=Pompiere" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Didact+Gothic" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Taviraj" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Playfair+Display" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<script src="bootstrap/js/bootstrap.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.13.0/popper.min.js"></script>
	<link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
</head>
<body>

	<?php require_once 'social-icon.php'; ?>
	<?php require_once 'nav.php'; ?>


	<!--// Section where the contact form is -->

	<div class="container">
		<div class="row" id="contact-row">
			<div class="col-12 col-sm-10 offset-sm-1 col-md-10 offset-md-1 col-lg-10 offset-lg-1">
				<form action="" method="POST">
					<div class="row">
						<div class="col-12 col-sm-12 col-md-6 col-lg-6">
							<div class="form-group">
							    <label for="exampleInputEmail1">Name *</label>
							    <input type="text" name="fname" required="" ="" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
							    <small id="emailHelp" class="form-text text-muted">First name</small>
							</div>
						</div>
						<div class="col-12 col-sm-12 col-md-6 col-lg-6">
							<div class="form-group">
							    <label for="exampleInputEmail"></label>
							    <input type="text" name="lname" class="form-control" id="exampleInputEmail2" aria-describedby="emailHelp">
							    <small id="emailHelp" class="form-text text-muted">Last name</small>
							</div>
						</div>
						<div class="col-12">
							<label for="exampleInputEmail1">Email address *</label>
							<input type="email" name="email" required="" class="form-control"  id="exampleInputEmail3" aria-describedby="emailHelp">
						</div>
						<div class="col-12">
							<label id="sub-label" for="exampleInputEmail1">Subject *</label>
							<input type="text" name="subject" required="" class="form-control" id="exampleInputEmail4" aria-describedby="emailHelp">
						</div>
						<div class="col-12">
							<label id="mess-label" for="exampleInputEmail1">Message *</label>
							<textarea class="form-control" required="" name="message" id="exampleInputEmail5" aria-describedby="emailHelp"></textarea>
						</div>
						<div class="col-12">
							<button type="submit" name="submit" class="btn btn-default">SUBMIT</button>
						</div>
					</div>	
				</form>

				<?php
						$messagez="";
						$subject="";
						$email="";
							if(isset($_POST["submit"])){
							$messagez = $_POST["message"];
							$subject = $_POST["subject"];
							$email = $_POST["email"];
							$to      =  'markotaskovic93@gmail.com';
							$subject = 	$subject;
							$message =  $messagez;
							$message .= " Message from ";
							$message .= $email;
							$headers = "From: Contact-Form@justgereige.com\r\n"; 
							mail($to, $subject, $message, $headers);
							}	
				?>
			</div> 	
		</div>
	</div>



	<!--// End contact form section -->


	<?php require_once 'footer.php'; ?>


	<!--<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/js/bootstrap.min.js" integrity="sha384-a5N7Y/aK3qNeh15eJKGWxsqtnX/wWdSZSKp+81YjTmS15nvnvxKHuzaWwXHDli+4" crossorigin="anonymous"></script>-->
</body>
</html>