<!DOCTYPE html>
<html>
<head>
	<title>About</title>
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" type="text/css" href="style/css/about.css">
    <link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="style/css/responsive.css">
    <link rel="stylesheet" type="text/css" href="style/css/social-box.css">
    <link rel="stylesheet" type="text/css" href="style/css/nav.css">
    <link rel="stylesheet" type="text/css" href="style/css/footer.css">
    <link href="https://fonts.googleapis.com/css?family=Didact+Gothic" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Playfair+Display" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Oranienbaum" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Taviraj" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
</head>
<body>

  <?php require_once 'social-icon.php'; ?>
	<?php require_once "nav.php"; ?>

<div id="carouselExampleSlidesOnly" class="carousel slide" data-ride="carousel">
  <div class="carousel-inner">
    <div class="carousel-item active">
      <div class="container-fluid" id="kont">
        <img id="image-zena" class="img-fluid" src="image/zena1.jpeg" alt="First slide">
        <div class="d-none d-xl-block header-text text-center">
      			<div class="centered container">
      				<div class="row">
                  <div class="offset-lg-8 col-lg-4 main_title">
		                <h1>Lorem ipsum</h1>
		                <div class="offset-lg-4 col-lg-4" id="underline"></div><br>
		                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                 </div>
             	</div>
            </div>
        </div><!-- /header-text -->
      </div>
        <div class="d-xl-none text text-center">
        	<div class="container-fluid" id="smaller-than-xl">
        		<div class="row">
        			<div class="col-xs-12 col-sm-12 col-md-12 main_title">
        				 <h1>Lorem ipsum</h1>
		                <div class="offset-5 col-2" id="underline"></div>
		                <h2>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</h2>
        			</div>
        		</div>
        	</div>
        </div>
    </div>
  </div>
</div>

	<?php require_once "footer.php"; ?>

<script src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.2.1.min.js"></script>
<script type="text/javascript" src="bootstrap/js/bootstrap.min.js"></script>
</body>
</html>