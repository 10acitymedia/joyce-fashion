<?php
	session_start();

	if($_SERVER['REQUEST_METHOD'] == "POST"){

		if($_POST['username'] == "admin" && $_POST['password'] == "admin"){

			$_SESSION['admin'] = "markotaskovic93@gmail.com";

			header("Location: dashboard.php");

		}else{

			$error = "Niste ulogovani";

		}

	}

?>

<!DOCTYPE html>
<html>
<head>
	<title>Dashboard Joyce</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="style/css/admin.css">
	<link href="https://fonts.googleapis.com/css?family=Pompiere" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Didact+Gothic" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<script src="bootstrap/js/bootstrap.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.13.0/popper.min.js"></script>
	<link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
</head>
<body>

	<div class="container-fluid" id="form-container">
		<div class="row">
			<div class="container">
				<div class="row text-center" id="form-row">
					<div class="col-12 col-sm-12 col-md-4 offset-md-4 col-lg-4 offset-lg-4">
						<h1 class="text-center">Log In</h1><br>
						<p class="text-center"><?php if(isset($error)){ echo $error; }else{} ?></p>
						<form action="<?php $_SERVER['PHP_SELF']; ?>" method="POST">
						  	<div class="form-group">
						    	<input type="text" name="username" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter username">
						  	</div>
						  	<div class="form-group">
						    	<input type="password" name="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
						  	</div>
						  	<button class="btn btn-primary">Log In</button>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>

</body>
</html>