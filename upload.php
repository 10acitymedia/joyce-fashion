<!DOCTYPE html>
<html>
<head>
	<title>Uploading Many Images Into Mysql</title>
	<!--responsive with devices-->
    <meta name='viewport' content='width=device-width, initial-scale=1, maximum-scale=1'>
     <!--bootstrap-->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap-theme.min.css">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
    <!--jquery-->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <!--file upload-->
    <link href="style/css/fileinput.css" media="all" rel="stylesheet" type="text/css" />
    <script src="style/js/fileinput.min.js" type="text/javascript"></script>
</head>
<body>
<center>
	<form enctype="multipart/form-data" action="debug.php" method="POST">
		<div class="form-group">
			<input type="file" id="file-1" class="file" multiple name="image[]">
		</div>
		<input type="submit" class="btn btn-lg btn-primary">
	</form>
</center>
<script type="text/javascript">
	$('#file-1').fileinput({
		uploadUrl:'#',
		allowedFileExtension:['jpg','png','gif'],
		overwriteInitial:false,
		maxFileSize:1000,
		maxFileNum:5,
	});
</script>

</body>
</html>